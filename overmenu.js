var overmenuState = {
  point:0,
  text:[],
  preload: function()
  {

  },

  create: function()
  {
      game.stage.backgroundColor = '#aaaaaa';

      this.text[0]=game.add.text(game.width/2-140, game.height/4, 'PLAY AGAIN',{ font: '50px Arial', fill: '#000000' });
      this.text[1]=game.add.text(game.width/2-140, game.height/4+60, 'BACK TO MENU',{ font: '50px Arial', fill: '#000000' });
      this.text[1].alpha=0.5;
      //this.text[0].alpha=0.5;

      this.cursor = game.input.keyboard.createCursorKeys();
      this.inputUp = game.input.keyboard.addKey(Phaser.Keyboard.UP);
      this.inputDown = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
      this.inputSpace = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
      this.point=0;


  },

  update: function()
  {
    this.change();
    console.log(this.point);
  },
  change: function()
  {
    if(this.inputDown.justDown){
      this.point=(this.point+1)%2;
      this.text[1].alpha=0.5;
      this.text[0].alpha=0.5;
      this.text[this.point].alpha=1.0;
    }
    if(this.inputUp.justDown){
      this.point=(this.point+1)%2;
      this.text[1].alpha=0.5;
      this.text[0].alpha=0.5;
      this.text[this.point].alpha=1.0;
    }
    if(this.inputSpace.justDown){
      if(this.point===0)
      {
        game.state.start('main');
      }
      if(this.point===1)
      {
        game.state.start('start');
      }
    }
  }

}
