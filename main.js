var main = {
  life:50,
  level:0,
  ot:0,
  vx:[0,-141,-200,-141,0,141,200,141],
  vy:[-200,-141,0,141,200,141,0,-141,],
  text:[],
 preload: function() {
   game.load.spritesheet('plane','assets/plane.png',80,90);
   game.load.physics('plane_shape_c','assets/pc.json');
   game.load.physics('foejson','assets/foejson.json');
   game.load.image('foe','assets/foe.png');
   game.load.image('ball','assets/ball.png');
   game.load.image('ballr','assets/ballr.png');
   game.load.image('foeball','assets/foeball.png');
   game.load.image('foeballr','assets/foeballr.png');
   game.load.image('background','assets/background2.png');
   game.load.image('pixelg', 'assets/pixelg.png');
   game.load.image('pixelp', 'assets/pixelp.png');
   game.load.audio('shot', 'assets/shot1.mp3');
   game.load.audio('bomb', 'assets/bomb1.mp3');
   //game.load.image('lazer', 'assets/lazer.png');
   //game.load.physics('plane_shape_r','assets/pr.json');
   //game.load.physics('plane_shape_l','assets/pl.json');
 },
 pause:function(){
   if(game.paused)
          game.paused=!game.paused;
 },
 create: function() {
   this.text[0]=game.add.text(10, game.height-140, 'Esc : pause , Click : unpause',{ font: '20px Arial', fill: '#000000' });
   this.text[1]=game.add.text(10, game.height-115, 'MUTE_flag:(press M to toggle)',{ font: '20px Arial', fill: '#000000' });
   this.text[2]=game.add.text(10, game.height-90, 'plane_life:',{ font: '20px Arial', fill: '#000000' });
   this.text[3]=game.add.text(10, game.height-65, 'ot:',{ font: '20px Arial', fill: '#000000' });
   console.log(this.text[0]);

   game.input.onDown.add(this.pause, self);
   this.level=0;
   this.ot=0;
   this.tileSprite = game.add.tileSprite(0,0,w,h,'background');
   this.tileSprite.autoScroll(0,100);
   game.stage.backgroundColor = '#aaaaaa';
   game.physics.startSystem(Phaser.Physics.P2JS);
   game.physics.p2.setImpactEvents(true);
   //game.renderer.renderSession.roundPixels = true;
   this.cursor = game.input.keyboard.createCursorKeys();

   this.plane = game.add.sprite(game.width/2, game.height-game.height/4, 'plane');
   this.plane2 = game.add.sprite(game.width/2, game.height/4, 'foe');
   this.plane.anchor.setTo(0.5, 0.5);
   this.plane2.anchor.setTo(0.5, 0.5);
   game.physics.p2.enable([this.plane],false);
   game.physics.p2.enable([this.plane2],false);

   this.plane.body.clearShapes();
   this.plane.body.loadPolygon('plane_shape_c', 'pc');
   this.plane2.body.clearShapes();
   this.plane2.body.loadPolygon('foejson', 'foe');
   this.plane2.body.fixedRotation = true;
   this.plane.body.fixedRotation = true;
   //console.log('reset life');
   this.life=50;

   //console.log(this.plane.body);

   this.plane.animations.add('plane_l',[2,2],16,false);
   this.plane.animations.add('plane_r',[3,3],16,false);
   this.plane.animations.add('plane_s',[0,1],8,true);

   this.inputM= game.input.keyboard.addKey(Phaser.Keyboard.M);

   this.inputA = game.input.keyboard.addKey(Phaser.Keyboard.A);
   this.inputW = game.input.keyboard.addKey(Phaser.Keyboard.W);
   this.inputS = game.input.keyboard.addKey(Phaser.Keyboard.S);
   this.inputD = game.input.keyboard.addKey(Phaser.Keyboard.D);
   this.inputB = game.input.keyboard.addKey(Phaser.Keyboard.B);
   this.inputESC = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
   this.inputSpace = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

   this.planeG = game.physics.p2.createCollisionGroup();
   this.enemyG = game.physics.p2.createCollisionGroup();
   this.ballG = game.physics.p2.createCollisionGroup();
   this.foeballG = game.physics.p2.createCollisionGroup();

   this.plane.body.setCollisionGroup(this.planeG);
   this.plane2.body.setCollisionGroup(this.enemyG);

   this.plane.body.collides([this.enemyG,this.foeballG]);
   this.plane2.body.collides([this.planeG,this.ballG],this.h,this);

   //this.plane2.body.onBeginContact.add(this.h);

   this.emitter = game.add.emitter(0, 0, 15);
   this.emitter.makeParticles('pixelp');
   this.emitter.setYSpeed(-150, 150);
   this.emitter.setXSpeed(-150, 150);
   this.emitter.setScale(2, 0, 2, 0, 1000);
   this.emitter.gravity = 0;

   this.shootemitter = game.add.emitter(0, 0, 20);
   this.shootemitter.makeParticles('pixelg');
   this.shootemitter.setYSpeed(-150, 0);
   this.shootemitter.setXSpeed(-300,300);
   this.shootemitter.setScale(1, 0, 1, 0, 300);
   this.shootemitter.gravity = 0;

   this.shotmusic=game.add.audio('shot');
   this.bombmusic=game.add.audio('bomb');
   this.shotmusic.volume=0.1;
   this.bombmusic.volume=0.1;
   var progressText = game.add.text(3, 950, '50%', {
    fontSize: '60px',
    fill: '#ffffff'
});
 },
 update: function() {
   //this.text[0].setText('haha');
   this.text[1].setText('MUTE_flag : '+this.shotmusic.mute);
   this.text[2].setText('plane_life : '+this.life);
   this.text[3].setText('ot : '+this.ot+' (since 1200 can use)(press B)');
    this.movePlayer();
    this.movePlayer2();
    if(foebullet&&this.plane2.alive)this.foeshoot();
    if(foecreate===true)
    {
      foecreate=false;
      plane2temp=this.plane2;
      this.level+=1;
      setTimeout(this.respawn,3000,this.level);
    }
    if(this.life<=0)
    {
      console.log('dead');
      game.state.start('gameover');
    }
    if(this.ot<1200)
    this.ot+=1;
    console.log(this.ot);
 },
 respawn:function(level){
    plane2temp.reset(game.width/2, game.height/4);
    console.log('level:'+level);
 },
 playeronhit: function(body1) {
    this.emitter.x = body1.x;
    this.emitter.y = body1.y;
    this.emitter.start(true, 800, null, 15);
    this.bombmusic.play();
    //console.log('boom1');
},
shootparticle: function() {
   this.shootemitter.x = this.plane.x;
   this.shootemitter.y = this.plane.y-45;
   this.shootemitter.start(true, 100, null, 20);
   //this.shotmusic.play();
   //console.log('boom2');
},
 foeshoot: function(){
   foebullet=false;
   //console.log(777);
   setTimeout(function(){ foebullet=true; }, 1200);
   if(this.level>=1)
   {
     this.foeball = game.add.sprite(this.plane2.body.x, this.plane2.body.y+45, 'foeballr');
   }
   else this.foeball = game.add.sprite(this.plane2.body.x, this.plane2.body.y+45, 'foeball');
   this.foeball.anchor.setTo(0.5, 0.5);
   game.physics.p2.enable(this.foeball,false);
   this.foeball.body.setCollisionGroup(this.foeballG);
   this.foeball.body.collides([this.planeG],(body1,body2)=>{
     this.life-=1;body1.sprite.kill();console.log(this.life);
     this.playeronhit(body1);
   },this);
   this.foeball.body.velocity.y=141;
   this.foeball.body.velocity.x=141;
   this.foeball.body.collideWorldBounds=false;
   // this.foeball.body.setCollisionGroup(this.ballG);
   // this.foeball.body.collides(this.enemyG);
   this.foeball.checkWorldBounds = true;
   console.log(this.foeball);
   this.foeball.outOfBoundsKill = true;
   this.foeball.body.type=8;
   if(this.level>=1)
   {
     setTimeout(this.boom8, 1000,this.foeball,this.planeG,this.foeballG,this.vx,this.vy,this.playeronhit,this.redboom,this);
   }

   if(this.level>=2)
   {
     this.foeball = game.add.sprite(this.plane2.body.x, this.plane2.body.y+45, 'foeballr');
   }
   else this.foeball = game.add.sprite(this.plane2.body.x, this.plane2.body.y+45, 'foeball');
   this.foeball.anchor.setTo(0.5, 0.5);
   game.physics.p2.enable(this.foeball,false);
   this.foeball.body.setCollisionGroup(this.foeballG);
   this.foeball.body.collides([this.planeG],(body1,body2)=>{this.life-=1;body1.sprite.kill();console.log(this.life);this.playeronhit(body1);},this);
   this.foeball.body.velocity.y=200;
   this.foeball.body.collideWorldBounds=false;
   // this.foeball.body.setCollisionGroup(this.ballG);
   // this.foeball.body.collides(this.enemyG);
   this.foeball.checkWorldBounds = true;
   this.foeball.outOfBoundsKill = true;
   this.foeball.body.type=8;
   if(this.level>=2)
   {
     setTimeout(this.boom8, 1000,this.foeball,this.planeG,this.foeballG,this.vx,this.vy,this.playeronhit,this.redboom,this);
   }

   if(this.level>=3)
   {
     this.foeball = game.add.sprite(this.plane2.body.x, this.plane2.body.y+45, 'foeballr');
   }
   else this.foeball = game.add.sprite(this.plane2.body.x, this.plane2.body.y+45, 'foeball');
   this.foeball.anchor.setTo(0.5, 0.5);
   game.physics.p2.enable(this.foeball,false);
   this.foeball.body.setCollisionGroup(this.foeballG);
   this.foeball.body.collides([this.planeG],(body1,body2)=>{this.life-=1;body1.sprite.kill();console.log(this.life);this.playeronhit(body1);},this);
   this.foeball.body.velocity.y=141;
   this.foeball.body.velocity.x=-141;
   this.foeball.body.collideWorldBounds=false;
   // this.foeball.body.setCollisionGroup(this.ballG);
   // this.foeball.body.collides(this.enemyG);
   this.foeball.checkWorldBounds = true;
   this.foeball.outOfBoundsKill = true;
   this.foeball.body.type=8;
   if(this.level>=3)
   {
     setTimeout(this.boom8, 1000,this.foeball,this.planeG,this.foeballG,this.vx,this.vy,this.playeronhit,this.redboom,this);
   }
 },
 redboom:function(body1,body2){
   console.log('h1h1hh1:'+this.life);
   //console.log('h1h1hh1:'+qqqq.life);
   body1.sprite.kill();
   this.playeronhit(body1);
   this.life-=1;
 },
 boom8:function(foeball, planeG, foeballG, vx, vy, playeronhit,redboom,qq){
   foeball.body.sprite.kill();
   for (i = 0; i < 8; i++)
   {
     qq.foeball = game.add.sprite(foeball.body.x, foeball.body.y, 'foeball');
     qq.foeball.anchor.setTo(0.5, 0.5);
     qq.physics.p2.enable(qq.foeball,false);
     qq.foeball.body.setCollisionGroup(foeballG);

     qq.foeball.body.collides(planeG,qq.redboom,qq);

     qq.foeball.body.velocity.y=vx[i];
     qq.foeball.body.velocity.x=vy[i];
     qq.foeball.checkWorldBounds = true;
     qq.foeball.body.collideWorldBounds=false;
     // this.foeball.body.setCollisionGroup(this.ballG);
     // this.foeball.body.collides(this.enemyG);
     qq.foeball.outOfBoundsKill = true;
     qq.foeball.body.type=8;
   }

 },
 h:function(plane,bullet)
 {
   if(bullet.type===7){
  bullet.sprite.kill();
  blood-=1;}
  if(bullet.type===10){
 bullet.sprite.kill();
 blood-=100;}
  //console.log(plane);
  //console.log(bullet);console.log(blood);

  if(blood<=0)
  {
    plane.sprite.kill();
    blood=50;
    foecreate=true;
  }
 },
 movePlayer: function() {

   if(this.inputM.justDown)
   {
     this.shotmusic.mute=!this.shotmusic.mute;
     this.bombmusic.mute=!this.bombmusic.mute;
   }

   if(this.inputESC.justDown)
   {
     game.paused=true;
   }

   if(this.inputB.justDown&&this.ot===1200)
   {
     //this.lazer = game.add.sprite(this.plane.body.x, this.plane.body.y-45, 'lazer');
     //this.lazer.anchor.setTo(0.5, 1);
     bullet=false;
     setTimeout(function(){ bullet=true; }, 100);
     this.ot=0;
     this.shotmusic.play();
     this.ball = game.add.sprite(this.plane.body.x, this.plane.body.y-45, 'ballr');
     this.ball.anchor.setTo(0.5, 0.5);
     game.physics.p2.enable(this.ball,false);
     this.ball.body.velocity.y=-1000;
     this.ball.body.collideWorldBounds=false;
     this.ball.body.setCollisionGroup(this.ballG);
     this.ball.body.collides(this.enemyG);
     this.ball.outOfBoundsKill = true;
     this.ball.body.type=10;
     this.shootparticle();

   }

      this.plane.body.velocity.x=0;
      this.plane.body.velocity.y=0;
   if (this.cursor.left.isDown&&this.plane.x>=0+45)
   {
      this.plane.body.velocity.x -= 350;
      // this.plane.animations.play('plane_l');
   }
   if(this.cursor.right.isDown&&this.plane.x<=w-45)
   {
      this.plane.body.velocity.x += 350;
      // this.plane.animations.play('plane_r');
   }
  if(this.cursor.up.isDown&&this.plane.y>=0+45)
      this.plane.body.velocity.y -= 350;
  if(this.cursor.down.isDown&&this.plane.y<=h-45)
      this.plane.body.velocity.y += 350;

  if(!(this.cursor.left.isDown||this.cursor.right.isDown||this.cursor.up.isDown||this.cursor.down.isDown)) {
      this.plane.body.velocity.x = 0;
      this.plane.body.velocity.y = 0;
    }
  if(this.inputSpace.isDown&&bullet)
  {
    bullet=false;
    setTimeout(function(){ bullet=true; }, 100);
    this.ball = game.add.sprite(this.plane.body.x, this.plane.body.y-45, 'ball');
    this.ball.anchor.setTo(0.5, 0.5);
    game.physics.p2.enable(this.ball,false);
    this.ball.body.velocity.y=-600;
    this.ball.body.collideWorldBounds=false;
    this.ball.body.setCollisionGroup(this.ballG);
    this.ball.body.collides(this.enemyG);
    this.ball.outOfBoundsKill = true;
    this.ball.body.type=7;
    this.shootparticle();
  }

  if(this.plane.body.velocity.x===-350)
    {
      this.plane.animations.stop();
      this.plane.animations.play('plane_l');
    }
    else if(this.plane.body.velocity.x===350)
    {
      this.plane.animations.stop();
      this.plane.animations.play('plane_r');
    }
    else
    {
      this.plane.animations.play('plane_s');
    }

  },
  movePlayer2: function() {
       // this.plane2.body.velocity.x=0;
       // this.plane2.body.velocity.y=0;
    if (this.inputA.isDown&&this.plane2.x>=0+45)
    {
       this.plane2.body.velocity.x -= 350;
       // this.plane.animations.play('plane_l');
    }
    if(this.inputD.isDown&&this.plane2.x<=w-45)
    {
       this.plane2.body.velocity.x += 350;
       // this.plane.animations.play('plane_r');
    }
   if(this.inputW.isDown&&this.plane2.y>=0+45)
       this.plane2.body.velocity.y -= 350;
   if(this.inputS.isDown&&this.plane2.y<=h-45)
       this.plane2.body.velocity.y += 350;

   if(!(this.inputA.isDown||this.inputD.isDown||this.inputS.isDown||this.inputW.isDown)) {
       this.plane2.body.velocity.x += 0;
       this.plane2.body.velocity.y += 0;
     }
    if(this.plane2.body.y>=game.height/4&&this.plane2.body.x<=game.width-55)
    {
      this.plane2.body.velocity.x = 200;
      this.plane2.body.velocity.y =0;
    }
    if(this.plane2.body.y>=55&&this.plane2.body.x>=game.width-55)
    {
      this.plane2.body.velocity.x =0;
      this.plane2.body.velocity.y =-200;
    }
    if(this.plane2.body.y<=55&&this.plane2.body.x>=55)
    {
      this.plane2.body.velocity.x =-200;
      this.plane2.body.velocity.y =0;
    }
    if(this.plane2.body.y<=game.height/4&&this.plane2.body.x<=55)
    {
      this.plane2.body.velocity.x =0;
      this.plane2.body.velocity.y =200;
    }

   }
};

var bullet=true;
var blood=50;
var w=700;
var h=800;
var foecreate=false;
var foebullet=true;
var plane2temp;
var game = new Phaser.Game(w, h+150, Phaser.AUTO, 'canvas');
game.state.add('main', main);
game.state.add('start', startState);
game.state.add('gameover', gameoverState);
game.state.add('overmenu', overmenuState);
game.state.start('start');
