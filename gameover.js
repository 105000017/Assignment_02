var gameoverState = {
  preload: function()
  {

  },

  create: function()
  {
      game.stage.backgroundColor = '#FF';

      game.add.text(60, game.height/4, 'GAMEOVER',{ font: '100px Arial', fill: 'red' });
      setTimeout(function(){ game.state.start('overmenu'); }, 2000);
  },

  update: function()
  {

  }

}
