# Software Studio 2019 Spring Assignment_02
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [105000017  plane_game]


## Basic
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects |5%|Y|
|Appearance|5%|Y|
|Leaderboard|5%|N|
|:-:|:-:|:-:|
|plus|80%~85%|//|
|:-:|:-:|:-:|
## Advanced
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Bonus|40%|N|

## planegame Detail Description

# 作品網址：[https://105000017.gitlab.io/Assignment_02]

# Jucify mechanisms(describe) :
1. 大招是按B 能直接殺死敵人 彈藥蓄力20秒
2. 敵人的子彈會炸裂出八顆子彈(乘以level)，隨著遊戲愈來愈困難
