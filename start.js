var startState = {
  point:0,
  text:[],
  preload: function()
  {

  },

  create: function()
  {
      game.stage.backgroundColor = '#aaaaaa';

      this.text[0]=game.add.text(game.width/2-60, game.height/4, 'START',{ font: '50px Arial', fill: '#000000' });
      this.text[1]=game.add.text(game.width/2-60, game.height/4+60, 'SETTING',{ font: '50px Arial', fill: '#000000' });
      this.text[2]=game.add.text(game.width/2-60, game.height/4+120, 'ABOUT',{ font: '50px Arial', fill: '#000000' });
	  this.text[3]=game.add.text(game.width/2-80, game.height/4+180, 'press space to start and shoot',{ font: '15px Arial', fill: '#000000' });
      this.text[1].alpha=0.5;
      this.text[2].alpha=0.5;

      this.cursor = game.input.keyboard.createCursorKeys();
      this.inputUp = game.input.keyboard.addKey(Phaser.Keyboard.UP);
      this.inputDown = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
      this.inputSpace = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);


  },

  update: function()
  {
    this.change();
  },
  change: function()
  {
    if(this.inputDown.justDown){
      this.point=(this.point+1)%3;
      this.text[1].alpha=0.5;
      this.text[2].alpha=0.5;
      this.text[0].alpha=0.5;
      this.text[this.point].alpha=1.0;
    }
    if(this.inputUp.justDown){
      this.point=(this.point+2)%3;
      this.text[1].alpha=0.5;
      this.text[2].alpha=0.5;
      this.text[0].alpha=0.5;
      this.text[this.point].alpha=1.0;
    }
    if(this.inputSpace.justDown){
      if(this.point===0)
      {
        game.state.start('main');
      }
    }
  }

}
